(() => {
    const CONTROL_LEFT = document.getElementById('control_slider--left')
    const CONTROL_RIGHT = document.getElementById('control_slider--right')
    const SLIDER_BLOCKS = document.querySelectorAll('.slider_block')
    

    
    CONTROL_LEFT.addEventListener('click',function (e){
        e.preventDefault()        
        SLIDER_BLOCKS.forEach(block => {
            block.style.left = '-50%'
        })
    })
    CONTROL_RIGHT.addEventListener('click',function (e){
        e.preventDefault()        
        SLIDER_BLOCKS.forEach(block => {
            block.style.left = '0%'
        })
    })
})()