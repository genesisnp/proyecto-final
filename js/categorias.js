(() => {
  const MENU_CATEGORIES = document.getElementById('menu-content-categories')
  const BTN_CATEGORIAS = document.getElementById('categories-select')
  let IS_OPEN_MENU = false

  // HELPERS
  var Helpers = {
    showMenu: () => {
      MENU_CATEGORIES.style.display = 'block'
    },
    hideMenu: () => {
      MENU_CATEGORIES.style.display = 'none'
    }
  }


  //EVENTS
  BTN_CATEGORIAS.addEventListener('click', () => {
    if (IS_OPEN_MENU) {
      Helpers.hideMenu()
    } else {
      Helpers.showMenu()
    }
    IS_OPEN_MENU = !IS_OPEN_MENU
    console.log(IS_OPEN_MENU)
  })
})()