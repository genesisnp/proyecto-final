const MODAL = document.querySelector('.modal')
const MODAL_BODY = document.querySelector('.modal-body')
const MODAL_BODY_IMG = document.querySelector('.modal-body-img')
const BTNS = document.querySelectorAll(".myBtn")
const BTN_CLOSE = document.querySelector(".close");

BTNS.forEach(button => {
    button.addEventListener('click', () => {
        const images = event.target.parentElement.children[0].src
        const title = event.target.parentElement.children[1].innerText
        const tecnica = event.target.parentElement.children[2].innerText
        const precio = event.target.parentElement.children[3].innerText
        const author = event.target.parentElement.children[4].innerText
        const formato = event.target.parentElement.children[5].innerText
        const description = event.target.parentElement.children[6].innerText
        const obj = {
            title:title,
            tecnica: tecnica,
            autor: author,
            precio: precio,
            formato: formato,
            description: description,
            images: images
        }
        const maketemplate = (obj) => {
            return `
                <h1 class="title">TITULO:<span>${obj.title}</span></h1>
                <h1 class="tecnica">TÉCNICA: <span>${obj.tecnica}</span></h1>
                <h1 class="precio">PRECIO: <span>${obj.precio}</span></h1>
                <h1 class="autor">AUTOR:<span> ${obj.autor}</span></h1>
                <h1 class="formato">FORMATO: <span>${obj.formato}</span></h1>
                <h1 class="description">DESCRIPCION: <span>${obj.description}</span></h1>
            `
        }
        const maketemplateimages = (obj) => {
            return `
                <img class="images" src="${obj.images}">
            `
        }
        var TEMPLATE = maketemplate(obj);
        var TEMPLATE_IMAGES = maketemplateimages(obj);
        MODAL_BODY.innerHTML = TEMPLATE;
        MODAL_BODY_IMG.innerHTML = TEMPLATE_IMAGES;
        MODAL.style.display = 'block'
        BTN_CLOSE.style.display = 'block'


    })
})
BTN_CLOSE.addEventListener('click', function () {
    MODAL.style.display = "none";
})

