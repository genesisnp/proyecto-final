(() => {
  // VARIABLES GLOBALES
  const LOCATION_PATHNAME = location.pathname
  const IS_INDEX = (LOCATION_PATHNAME == '/' || LOCATION_PATHNAME == '/index.html') ? true : false
  const IMG_LOGO = document.getElementById('img-logo-index')
  const HEADER = document.getElementById('header-index')
  const WINDOW_SIZE = window.innerWidth
  const LOGO_SIZE = WINDOW_SIZE < 768 ? '100px' : '155px'

  // EVENTOS
  if (IS_INDEX) {
    window.addEventListener('scroll', function (e) {
      
      if (window.pageYOffset > 112) {
        Object.assign(IMG_LOGO.style, {
          width: '90px',
        })
        
        Object.assign(HEADER.style, {
          backgroundColor: 'white',
          boxShadow:'0 4px 4px 0px #00000066'
        })
        IMG_LOGO.src = '../img/logo/artistas-peruanos.svg'
      } else {
        Object.assign(IMG_LOGO.style, {
          width: LOGO_SIZE,
        })
        
        Object.assign(HEADER.style, {
          backgroundColor: 'transparent',
          boxShadow:''
        })
        IMG_LOGO.src = '../img/logo/artistas-peruanos-white.svg'
      } 
    })
  }

})()


